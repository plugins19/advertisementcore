## Download link
```
"com.dikiyhimik.zenject": "https://gitlab.com/plugins19/zenject.git#upm",
"com.attaboysgames.yandexgamebuildpostprocessor" : "https://gitlab.com/plugins19/yandexgamesbuildpostprocessor.git#upm",
"com.attaboysgames.advertisementcore" : "https://gitlab.com/plugins19/advertisementcore.git#upm",
```


## Instruction

### AdMob ad

1. To add AdMob package into your project. [(Link for downloading)](https://github.com/googleads/googleads-mobile-unity)
2. Open "Game" scene
3. Realize AdMobSettingDatabase (AssetMenu: Databases/AdSettings/AdMobSettingsDatabase)
4. Add AdInstaller to Zenject context
5. Add AdMobSettingDatabase to AdInstaller.
6. Choose correct platform type in setting database. 
