mergeInto(LibraryManager.library, 
{
  ShowYandexInterstitialInternal: function()
  {
    console.log("Interstitial launch");
      ysdk.adv.showFullscreenAdv(
      {
        callbacks: 
        {
          onClose: function(wasShown)
          {
            if (wasShown) 
            {
              myGameInstance.SendMessage('YandexSDKHelper', 'InterstitialCallback', 'Success');
            }
            else
            {
              myGameInstance.SendMessage('YandexSDKHelper', 'InterstitialCallback', 'NoneShown');
            }

            console.log("Interstitial closed");
            console.log(wasShown);
          },
          onError: function(error) 
          {
            myGameInstance.SendMessage('YandexSDKHelper', 'InterstitialCallback', 'Error');
            console.log("Interstitial error");
          },
          onOffline: function() 
          {
            myGameInstance.SendMessage('YandexSDKHelper', 'InterstitialCallback', 'Offline');
            console.log("Interstitial error");
          },
        }
      })
  },

  ShowYandexRewardedVideoInternal: function()
  {
    console.log("Rewarded video launch");
    ysdk.adv.showRewardedVideo(
    {
        callbacks: 
        {
            onRewarded: function()
            {
              myGameInstance.SendMessage('YandexSDKHelper', 'RewardedVideoCallback', 'Rewarded');
              console.log('Rewarded video rewarded.');
            },
            onClose: function()
            {
              myGameInstance.SendMessage('YandexSDKHelper', 'RewardedVideoCallback', 'Closed');
              console.log('Rewarded video closed.');
            },
            onError: function(e)
            {
              myGameInstance.SendMessage('YandexSDKHelper', 'RewardedVideoCallback', 'Error');
              console.log('Rewarded video error.');
            },
        }
    })
  },

  ShowBannerInternalFromSDK: function()
  {
    console.log("Banner shown");
    ysdk.adv.showBannerAdv()
  },

  HideBannerInternalFromSDK: function()
  {
    console.log("Banner hidden");
    ysdk.adv.hideBannerAdv()
  },

});