using System;
using Runtime.AdWrappers.RewardedVideo;
using Runtime.AdWrappers.RewardedVideo.Strategies.Impls;
using UnityEngine;

namespace Runtime.Helper.YandexSDK
{
    public partial class YandexSDKHelper
    {
        public void RewardedVideoCallback(string rewardedVideoResult)
        {
            var result = (ERewardedVideoResult)Enum.Parse(typeof(ERewardedVideoResult), rewardedVideoResult);

            switch (result)
            {
                case ERewardedVideoResult.None:
                    Debug.LogError(
                        $"{typeof(YandexSDKHelper)} Incorrect rewarded video callback result: {rewardedVideoResult}"
                    );
                    break;
                case ERewardedVideoResult.Rewarded:
                    GetRewardedVideoWrapper().ExecuteRewarded();
                    break;
                case ERewardedVideoResult.Error:
                    GetRewardedVideoWrapper().ExecuteError();
                    break;
                case ERewardedVideoResult.Closed:
                    GetRewardedVideoWrapper().ExecuteClosed();
                    break;
            }
        }

        private YandexGameRewardedVideoStrategy GetRewardedVideoWrapper() =>
            _yandexGameRewardedVideoStrategy as YandexGameRewardedVideoStrategy;
    }
}