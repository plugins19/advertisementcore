using System;
using Runtime.AdWrappers.Interstitial;
using Runtime.AdWrappers.Interstitial.Strategies.Impls;
using UnityEngine;

namespace Runtime.Helper.YandexSDK
{
    public partial class YandexSDKHelper
    {
        public void InterstitialCallback(string interstitialResult)
        {
            var result = (EInterstitialResult)Enum.Parse(typeof(EInterstitialResult), interstitialResult);
            
            switch (result)
            {
                case EInterstitialResult.None:
                    Debug.LogError(
                        $"{typeof(YandexSDKHelper)} Incorrect interstitial callback result: {interstitialResult}"
                    );
                    break;
                case EInterstitialResult.Success:
                    GetInterstitialWrapper().ExecuteSuccess();
                    break;
                case EInterstitialResult.Error:
                    GetInterstitialWrapper().ExecuteError();
                    break;
                case EInterstitialResult.Offline:
                    GetInterstitialWrapper().ExecuteOffline();
                    break;
                case EInterstitialResult.NoneShown:
                    GetInterstitialWrapper().ExecuteNonShown();
                    break;
            }
        }

        private YandexGameInterstitialStrategy GetInterstitialWrapper() =>
            _yandexGameInterstitialStrategy as YandexGameInterstitialStrategy;
    }
}