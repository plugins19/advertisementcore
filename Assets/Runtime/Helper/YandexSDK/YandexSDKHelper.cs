using Runtime.AdWrappers.Interstitial.Strategies;
using Runtime.AdWrappers.RewardedVideo.Strategies;
using UnityEngine;
using Zenject;

namespace Runtime.Helper.YandexSDK
{
    public partial class YandexSDKHelper : MonoBehaviour
    {
        [Inject] private IYandexGameRewardedVideoStrategy _yandexGameRewardedVideoStrategy;
        [Inject] private IYandexGameInterstitialStrategy _yandexGameInterstitialStrategy;
    }
}