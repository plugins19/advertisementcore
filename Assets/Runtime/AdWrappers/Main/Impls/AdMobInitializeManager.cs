using System;
using Runtime.AdWrappers.AppOpen;
using Runtime.AdWrappers.InitializeStrategies;

namespace Runtime.AdWrappers.Main.Impls
{
    public class AdMobInitializeManager : IAdInitializeManager, IDisposable
    {
        private readonly IAppOpenManager _appOpenManager;
        private readonly IAdInitializeStrategy _adInitializeStrategy;
        
        public event Action<bool> AppOpenWasShown;
        
        public AdMobInitializeManager(
            IAppOpenManager appOpenManager,
            IAdInitializeStrategy adInitializeStrategy
        )
        {
            _appOpenManager = appOpenManager;
            _adInitializeStrategy = adInitializeStrategy;
        }

        public void Initialize()
        {
            _adInitializeStrategy.Initialize();
            _adInitializeStrategy.OnInitialized += OnInitialized;
        }

        public void Dispose()
        {
            _adInitializeStrategy.OnInitialized -= OnInitialized;
            _appOpenManager.OnShownSucceed -= OnAppOpenWasShown;
        }

        private void OnInitialized()
        {
            _appOpenManager.OnShownSucceed += OnAppOpenWasShown;
            _appOpenManager.PreloadAndShowAppOpen();
        }

        private void OnAppOpenWasShown(bool value) => AppOpenWasShown?.Invoke(value);
    }
}