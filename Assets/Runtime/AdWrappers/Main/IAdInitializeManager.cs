using System;
using Zenject;

namespace Runtime.AdWrappers.Main
{
    public interface IAdInitializeManager : IInitializable
    {
        event Action<bool> AppOpenWasShown;
    }
}