using System;
using GoogleMobileAds.Api;
using Runtime.Settings;
using UnityEngine;

namespace Runtime.AdWrappers.AppOpen.Impls
{
    public class AdMobAppOpenManager : IAppOpenManager
    {
        private readonly IAdSettingsDatabase _adSettingsDatabase;

        private AppOpenAd _appOpenAd;

        public event Action<bool> OnShownSucceed;

        public AdMobAppOpenManager(
            IAdSettingsDatabase adSettingsDatabase
        )
        {
            _adSettingsDatabase = adSettingsDatabase;
        }

        public void PreloadAndShowAppOpen()
        {
            Debug.Log("App open is loading");
            var appOpenId = _adSettingsDatabase.GetAppOpenId();
            var adRequest = new AdRequest();
            adRequest.Keywords.Add("unity-admob-app-open");

            void OnAppOpenLoad(AppOpenAd ad, AdError error)
            {
                if (error != null)
                {
                    Debug.Log($"App open loading error: {error.GetMessage()}");
                    OnShownSucceed?.Invoke(false);
                    return;
                }

                Debug.Log("App open was loaded");
                OnShownSucceed?.Invoke(true);
                _appOpenAd = ad;
                ShowAppOpen();
            }

            AppOpenAd.Load(appOpenId, ScreenOrientation.Portrait, adRequest, OnAppOpenLoad);
        }

        private void ShowAppOpen()
        {
            _appOpenAd.OnAdFullScreenContentClosed += OnClosed;
            _appOpenAd.OnAdFullScreenContentFailed += OnFail;
            _appOpenAd.Show();
        }

        private void OnClosed()
        {
            _appOpenAd.OnAdFullScreenContentClosed -= OnClosed;
            _appOpenAd.OnAdFullScreenContentFailed -= OnFail;
            Debug.Log($"App open was shown");
            _appOpenAd.Destroy();
            _appOpenAd = null;
            OnShownSucceed?.Invoke(true);
        }

        private void OnFail(AdError error)
        {
            _appOpenAd.OnAdFullScreenContentClosed -= OnClosed;
            _appOpenAd.OnAdFullScreenContentFailed -= OnFail;
            Debug.Log($"App open showing error: {error.GetMessage()}");
            _appOpenAd.Destroy();
            OnShownSucceed?.Invoke(false);
        }
    }
}