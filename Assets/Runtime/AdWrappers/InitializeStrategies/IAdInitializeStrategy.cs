using System;

namespace Runtime.AdWrappers.InitializeStrategies
{
    public interface IAdInitializeStrategy
    {
        event Action OnInitialized; 
        
        void Initialize();
    }
}