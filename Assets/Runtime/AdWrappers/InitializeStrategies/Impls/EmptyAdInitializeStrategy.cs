using System;
using UnityEngine;

namespace Runtime.AdWrappers.InitializeStrategies.Impls
{
    public class EmptyAdInitializeStrategy : IAdInitializeStrategy
    {
        public event Action OnInitialized;
        
        public void Initialize()
        {
            Debug.Log("Ad module initialized");
            OnInitialized?.Invoke();
        }
    }
}