using System;
using Runtime.AdWrappers.RewardInterstitial.Strategies;

namespace Runtime.AdWrappers.RewardInterstitial
{
	public interface IRewardedInterstitialWrapper
	{
		event Action OnAdLoaded; 
            
		bool IsLoaded { get; }

		void Preload();
        
		void Show(Action onComplete, Action onClose = null, Action onError = null);

		void ChangeStrategy(IRewardedInterstitialStrategy strategy);
	}
}