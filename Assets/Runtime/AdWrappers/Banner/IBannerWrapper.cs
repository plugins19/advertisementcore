namespace Runtime.AdWrappers.Banner
{
	public interface IBannerWrapper
	{
		bool IsBannerActive { get; }

		float Width { get; }

		float Height { get; }

		void ShowBanner(
			EBannerPosition bannerPosition = EBannerPosition.Bottom,
			EBannerType type = EBannerType.Banner,
			int width = 0,
			int height = 0
		);

		void HideBanner();

		void SetPosition(EBannerPosition position);

		void SetPosition(int x, int y);

		void DestroyBanner();

		void ChangeStrategy(IBannerStrategy strategy);
	}
}