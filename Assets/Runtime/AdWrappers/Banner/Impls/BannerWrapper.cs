namespace Runtime.AdWrappers.Banner.Impls
{
	public class BannerWrapper : IBannerWrapper
	{
		private IBannerStrategy _bannerStrategy;

		public bool IsBannerActive => _bannerStrategy.IsBannerActive;

		public float Width => _bannerStrategy.Width;

		public float Height => _bannerStrategy.Height;

		public BannerWrapper(
			IBannerStrategy bannerStrategy
		)
		{
			_bannerStrategy = bannerStrategy;
		}

		public void ShowBanner(
			EBannerPosition bannerPosition = EBannerPosition.Bottom,
			EBannerType type = EBannerType.Banner,
			int width = 0,
			int height = 0
		) => _bannerStrategy.ShowBanner(bannerPosition, type, 0, 0);

		public void HideBanner() => _bannerStrategy.HideBanner();

		public void SetPosition(EBannerPosition position) => _bannerStrategy.SetPosition(position);

		public void SetPosition(int x, int y) => _bannerStrategy.SetPosition(x, y);

		public void DestroyBanner() => _bannerStrategy.DestroyBanner();

		public void ChangeStrategy(IBannerStrategy strategy) => _bannerStrategy = strategy;
	}
}