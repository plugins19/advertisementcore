namespace Runtime.AdWrappers.Banner
{
    public enum EBannerPosition : byte
    {
        Top,
        Bottom,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        Center,
    }
}