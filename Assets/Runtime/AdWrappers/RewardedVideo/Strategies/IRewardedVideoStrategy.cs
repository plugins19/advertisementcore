using System;

namespace Runtime.AdWrappers.RewardedVideo.Strategies
{
    public interface IRewardedVideoStrategy
    {
        event Action OnAdLoaded;
        
        bool IsLoaded { get; }

        void Preload();
        
        void Show(Action onComplete, Action onClose = null, Action onError = null);
    }
}