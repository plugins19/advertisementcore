namespace Runtime.AdWrappers.RewardedVideo
{
    public enum ERewardedVideoResult : ushort
    {
        None = 0,
        Rewarded = 1,
        Error = 2,
        Closed = 3,
    }
}