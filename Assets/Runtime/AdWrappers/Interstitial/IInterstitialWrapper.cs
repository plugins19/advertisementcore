using System;
using Runtime.AdWrappers.Interstitial.Strategies;

namespace Runtime.AdWrappers.Interstitial
{
	public interface IInterstitialWrapper
	{
		event Action OnAdLoaded;
        
		bool IsLoaded { get; }
        
		void Preload();

		void Show(Action onComplete, Action onError = null);

		void ChangeStrategy(IInterstitialStrategy strategy);
	}
}