using System;
using Runtime.AdWrappers.Interstitial.Strategies;

namespace Runtime.AdWrappers.Interstitial.Impls
{
	public class InterstitialWrapper : IInterstitialWrapper, IDisposable
	{
		private IInterstitialStrategy _interstitialStrategy;

		public event Action OnAdLoaded;
		
		public bool IsLoaded => _interstitialStrategy.IsLoaded;

		public InterstitialWrapper(
			IInterstitialStrategy interstitialStrategy
		)
		{
			_interstitialStrategy = interstitialStrategy;
			_interstitialStrategy.OnAdLoaded += OnAdLoadedEvent;
		}

		public void Dispose() => _interstitialStrategy.OnAdLoaded -= OnAdLoadedEvent;

		public void Preload() => _interstitialStrategy.Preload();

		public void Show(Action onComplete, Action onError = null) => _interstitialStrategy.Show(onComplete, onError);

		public void ChangeStrategy(IInterstitialStrategy strategy)
		{
			_interstitialStrategy.OnAdLoaded -= OnAdLoadedEvent;
			_interstitialStrategy = strategy;
			_interstitialStrategy.OnAdLoaded += OnAdLoadedEvent;
		}

		private void OnAdLoadedEvent() => OnAdLoaded?.Invoke();
	}
}