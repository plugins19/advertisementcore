namespace Runtime.AdWrappers
{
    public enum EAdSourceType : ushort
    {
        Editor = 0,
        YandexGames = 1,
        AdMob = 2,
    }
}